#!/usr/bin/python3

import dbus.service
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib
from core.dbmanager import get_meaning
DBusGMainLoop(set_as_default=True)

objpath = "/hamsang_krunner"

iface = "org.kde.krunner1"
call_words = ['hamsang', 'همسنگ']

class Runner(dbus.service.Object):
    def __init__(self):
        dbus.service.Object.__init__(self, dbus.service.BusName("org.kde.hamsang_krunner", dbus.SessionBus()), objpath)

    @dbus.service.method(iface, in_signature='s', out_signature='a(sssida{sv})')
    def Match(self, query: str):
        """This method is used to get the matches and it returns a list of tupels"""
        first_word = query.split(' ')[0]
        if first_word in call_words:
            word = ' '.join(query.split(' ')[1:])
            definitions = get_meaning(word)
            answer = []
            for entry in definitions:
                dict_name = entry['dict_name']
                for record in entry['result']:
                    word = record['word']
                    definition = record['definition']
                    context = record['context']
                    # data, display text, icon, type (Plasma::QueryType), relevance (0-1), properties (subtext, category and urls)
                    answer.append(("Hello", f"{definition}",
                                   "languages", 100, 1.0,
                                   {'subtext': f'{dict_name}{":" if len(context) > 1 else ""}{context}'}))
            return answer

        return []

    @dbus.service.method(iface, out_signature='a(sss)')
    def Actions(self):
        # id, text, icon
        return [("id", "Tooltip", "planetkde")]

    @dbus.service.method(iface, in_signature='ss')
    def Run(self, data: str, action_id: str):
        print(data, action_id)


runner = Runner()
loop = GLib.MainLoop()
loop.run()
