#!/bin/bash

# Exit if something fails
set -e

mkdir -p ~/.local/share/kservices5/
mkdir -p ~/.local/share/dbus-1/services/

cp plasma-runner-hamsang_krunner.desktop ~/.local/share/kservices5/
sed "s|/home/erfan/projects/hamsang_krunner/hamsang_krunner.py|${PWD}/hamsang_krunner.py|" "org.kde.hamsang_krunner.service" > ~/.local/share/dbus-1/services/org.kde.hamsang_krunner.service

kquitapp5 krunner
