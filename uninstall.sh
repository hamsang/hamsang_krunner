#!/bin/bash

# Exit if something fails
set -e

rm ~/.local/share/kservices5/plasma-runner-hamsang_krunner.desktop
rm ~/.local/share/dbus-1/services/org.kde.hamsang_krunner.service
kquitapp5 krunner
